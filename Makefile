PIPENV := PIPENV_VENV_IN_PROJECT=1 pipenv --bare

# Note: This intentionally does not create/update Pipfile.lock.
# If dependencies have changed, then use the venv_dev target to perform a full update.
venv:
	$(PIPENV) sync

venv_dev: Pipfile.lock
	$(PIPENV) sync --dev
