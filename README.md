# Python example using Flask

## Pre-requisite
* Should know the basics of Python
* Install pipenv globally
```shell
$ pip install pipenv
```

## Checkout & Build

```
* `git clone` git@gitlab.com:enterpriseexamples/python_projects/python-flask.git
* `cd` python-flask
```

_Optional_: Install environment with contents from Pipfile. Generates Pipfile.lock
```shell
pipenv install
```

Create virtual environment by downloading libraries from Pipfile.lock
```shell
make venv_dev
```

## Run the apps
Since there are multiple examples, you can run it as follows:
* `python 01_tutorial.py`
* `python 02_tutorial.py`
* ....and so on....to test each one of them.

### Misc
* Find out where your virtual environment is:
```shell
$ pipenv --venv
```
This should show the `.venv` is within your project directory

* Find out where your project home is:
```shell
$ pipenv --where
```

## Reference
* [Python Spot](https://pythonspot.com/flask-web-app-with-python/)
* [Udemy Python-Flask](https://www.udemy.com/course/beginners-guide-flask-web-framework/)
