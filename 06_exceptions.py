from flask import Flask

app = Flask(__name__)


@app.route('/ex1')
def ex1_method():
    return _generate_exception1()


@app.route('/ex2')
def ex2_method():
    return _generate_exception2()


def _generate_exception1():
    try:
        return 12/0
    except: # default is "Exception"
        return "1- Some Exception happened "


def _generate_exception2():
    try:
        return 12/0
    except ZeroDivisionError as err:
        return "2- Exception happened " + str(err)
    except Exception as err:
        return "2- Some generic Exception happened " + err


if __name__ == '__main__':
    app.run()
