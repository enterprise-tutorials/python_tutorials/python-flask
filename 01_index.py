from flask import Flask

app = Flask(__name__)


@app.route('/', methods=["GET", "POST"])
def index():
    return "Hello World", 200


@app.route('/name', methods=["GET"])
def get_name() -> str:
    return "This is a 'TestName'"


@app.route('/name/<name1>', methods=["GET"])
def set_name(name1):
    return 'Your name is %s' % name1


@app.route('/age/<int:age1>', methods=["GET"])
def get_age(age1):
    return 'Your age is %d' % age1, 300


if __name__ == '__main__':
    app.run(port=8080)
