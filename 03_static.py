from flask import Flask, render_template

app = Flask(__name__)

# IMPORTANT: Python expects the static files to be under "static" folder


@app.route('/', methods=["GET"])
def index():
    return render_template('index.html')


@app.route('/static')
def static_method():
    return render_template('static.html')


if __name__ == '__main__':
    app.run()
