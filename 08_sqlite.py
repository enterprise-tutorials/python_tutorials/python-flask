from flask import Flask, render_template
import sqlite3

app = Flask(__name__)


@app.route('/')
def index_method():
    # If you are on Mac, by default sqlite is installed
    conn = sqlite3.connect("08tutorial.db")
    conn.execute('create table student(name text)')
    return render_template('08_content.html', msg="DB and table created")


@app.route('/create/<name>')
def add_record(name):
    with sqlite3.connect("08tutorial.db") as conn:
        cur = conn.cursor()
        cur.execute("insert into student (name) values (?)", [name])
        conn.commit()

    return render_template('08_content.html', msg="Record %s created" % name)


@app.route('/read')
def read_record():
    with sqlite3.connect("08tutorial.db") as conn:
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute("select * from student")
        rows = cur.fetchall()

    return render_template('08_content_read.html', row_content=rows)


if __name__ == '__main__':
    app.run()
