from flask import Flask, request

app = Flask(__name__)


@app.route('/', methods=["POST"])
def index_post():
    if request.data:
        return _read_data(request.get_json())


# NOTE: Any method that starts with "_" is private in Python
def _read_data(data):
    return data['name']


if __name__ == '__main__':
    app.run()
