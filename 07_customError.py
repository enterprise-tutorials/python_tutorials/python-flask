from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def index_method():
    return "Text from index_method" + 43


@app.errorhandler(500)
def error_handler_method(err):
    return render_template('error.html')


if __name__ == '__main__':
    app.run()
