from flask import Flask, redirect, url_for, abort

app = Flask(__name__)


@app.route('/', methods=["GET"])
def index_method():
    return "Hello World!"


@app.route('/<name>/<password>', methods=["GET"])
def read_data(name, password):
    if name == 'admin' and password == "pass":
        return redirect(url_for('admin_method'))
    else:
        abort(401);


@app.route('/admin', methods=["GET"])
def admin_method():
    return "This is from Admin dashboard"


if __name__ == '__main__':
    app.run()
